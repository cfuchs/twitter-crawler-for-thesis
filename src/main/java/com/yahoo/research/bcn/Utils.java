package com.yahoo.research.bcn;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class Utils {

    public static Twitter getTwitterInstance() {
        ConfigurationBuilder cb = new ConfigurationBuilder().setDebugEnabled(true);
        cb.setOAuthConsumerKey("Q0dWDTfZ3vt8aWvy5k4BA");
        cb.setOAuthConsumerSecret("N9nYSeYwzprdnLjhnpJyLcVtsuG3GK8isCC3oIq1U");
        cb.setOAuthAccessToken("18213426-MJZMIA8iWPeBwRreVLaDOoCPOAeUqewvPPrF9tGSY");
        cb.setOAuthAccessTokenSecret("PIQUm6s19k4XktkSkh7x27UECy6VGAlPpoWd9qrIc");;
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        return twitter;
    }
    
    /**
	 * Source:
	 * http://stackoverflow.com/questions/453018/number-of-lines-in-a-file
	 * -in-java
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static int countLines(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}
}
