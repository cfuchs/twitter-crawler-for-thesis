package com.yahoo.research.bcn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import twitter4j.CursorSupport;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

import com.google.common.collect.Lists;

public class TwitterCrawler {

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.err.println("Usage: Crawler <input_file> <output_file>");
            System.exit(1);
        }

        String infile = args[0];
        String outfile = args[1];
        BufferedReader in = new BufferedReader(new FileReader(infile));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.crawl();
    }

    private Twitter twitter = Utils.getTwitterInstance();
    private BufferedReader reader;
    private PrintWriter writer;
    private Map<Integer, Integer> errorMap = new HashMap<Integer, Integer>();

    private static final int MAX_RETRIES = 2;
    // state variables
    private long currentUserID;
    private long nextUserID = 0; // initialized for the first call to hasNextUser()
    private long currentCursor;
    private long currentCursorFollowers;
    private IDs currentFriends;
    private IDs currentFollowers;
    private int consecutiveErrors;
    private boolean retry;
    private boolean crawlFriends;
    private boolean crawlFollowers;
    
    

    public TwitterCrawler(BufferedReader in, PrintWriter out) throws IOException {
        this.reader = in;
        this.writer = out;
        this.nextUser(); // load first user
        this.setCrawlFollowers(true);
        this.setCrawlFriends(true);
    }
    
    public void crawl() throws IOException {
    	try {
            while (this.hasNextUser()) {
            	this.nextUser();
            	
            	if (isCrawlFriends())
            		crawlFriends();
                if (isCrawlFollowers())
                	crawlFollowers();
                
            }
        } finally {
        	this.close();
        }
    }

	private void crawlFollowers() {
		boolean done;
		// Crawl all followers of the current user
		System.out.println("Crawling followers of user " + this.getCurrentUserID());
		do {
		    done = false;
		    while (!done && this.retry()) {
		        try {
		            done = this.crawlNextFollowers();
		        } catch (TwitterException e) {
		        	this.handleTwitterException(e);
		        }
		    }
		} while (this.hasMoreFollowerPages() && this.retry());
		if (!done) {
		    System.out.println("Could not access followers of " + this.getCurrentUserID());
		}
	}

	private void crawlFriends() {
		boolean done;
		// Crawl all friends of the current user
		System.out.println("Crawling friends of user " + this.getCurrentUserID());
		do {
		    done = false;
		    while (!done && this.retry()) {
		        try {
		            done = this.crawlNextFriends();
		        } catch (TwitterException e) {
		        	this.handleTwitterException(e);
		        }
		    }
		} while (this.hasMoreFriendPages() && this.retry());
		if (!done) {
		    System.out.println("Could not access friends of " + this.getCurrentUserID());
		}
	}

    public void close() {
        writer.close();
        System.err.println("Error summary:\n" + errorMap);
    }

    // true = ok, false = exception
    public boolean crawlNextFriends() throws TwitterException {
        this.currentFriends = twitter.getFriendsIDs(getCurrentUserID(), currentCursor);
        for (long friendID : currentFriends.getIDs())
            writer.println(String.format("%d %d", getCurrentUserID(), friendID));
        writer.flush();
        if (currentFriends.hasNext())
            currentCursor = currentFriends.getNextCursor();
        return true;
    }
    
    // true = ok, false = exception
    public boolean crawlNextFollowers() throws TwitterException {
        this.currentFollowers = twitter.getFollowersIDs(getCurrentUserID(), currentCursorFollowers);
        for (long followerID : currentFollowers.getIDs())
            writer.println(String.format("%d %d", followerID, getCurrentUserID()));
        writer.flush();
        if (currentFollowers.hasNext())
        	currentCursorFollowers = currentFollowers.getNextCursor();
        return true;
    }

    public long getCurrentUserID() {
        return currentUserID;
    }
    
    public ResponseList<Status> crawlTimeline(long id) throws TwitterException {
    	return twitter.getUserTimeline(id);
    }
    
    public ResponseList<Status> crawlTimeline(long id, Paging paging) throws TwitterException {
    	return twitter.getUserTimeline(id, paging);
    }

    public void handleTwitterException(TwitterException e) {
    	if (e.exceededRateLimitation()) {
            logException(e);
            int secondsToSleep = 900; // 15 minutes
            int millisToSleep = 1000 * secondsToSleep;
            System.out.println("[" + new Date() + "] Sleeping for " + secondsToSleep + " seconds");
            long before = System.currentTimeMillis();
            try {
                Thread.sleep(millisToSleep);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            long now = System.currentTimeMillis();
            System.out.println("[" + new Date() + "] Woke up! Slept for " + (now - before) / 1000 + " seconds");
        } else {
            logException(e);
            if (consecutiveErrors++ > MAX_RETRIES)
                setRetry(false); // already tried enough
        }
    }
    
    public boolean hasMoreFriendPages() {
        return (currentFriends != null && currentFriends.hasNext());
    }
    
    public boolean hasMoreFollowerPages() {
        return (currentFollowers != null && currentFollowers.hasNext());
    }

    public boolean hasNextUser() throws IOException {
        return nextUserID >= 0;
    }

    private void logException(TwitterException e) {
        // print exception
        System.err.println(e.getMessage());
        // record it for statistics
        int code = e.getStatusCode();
        if (!errorMap.containsKey(code))
            errorMap.put(code, 0);
        errorMap.put(code, errorMap.get(code) + 1);
    }

    public void nextUser() throws IOException {
        currentUserID = nextUserID;
        String line = reader.readLine();
        if (line == null)
            nextUserID = -1;
        else
            nextUserID = Long.parseLong(line);
        currentFriends = null;
        currentFollowers = null;
        currentCursor = CursorSupport.START;
        currentCursorFollowers = CursorSupport.START;
        resetRetry();
        writer.flush();
    }

    public boolean retry() {
        return retry;
    }

    private void setRetry(boolean retry) {
        this.retry = retry;
    }

    private void resetRetry() {
        consecutiveErrors = 0; // reset retry counter
        retry = true;
    }

	public boolean isCrawlFriends() {
		return crawlFriends;
	}

	public void setCrawlFriends(boolean crawlFriends) {
		this.crawlFriends = crawlFriends;
	}

	public boolean isCrawlFollowers() {
		return crawlFollowers;
	}

	public void setCrawlFollowers(boolean crawlFollowers) {
		this.crawlFollowers = crawlFollowers;
	}

	public void userLookup() throws IOException {
		writer.println(String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s", "Id", "screenName", "name", "followersCount", "friendsCount", "realDegree", "statusesCount"));
		
		String line = null;
		ArrayList<Long> lineList = new ArrayList<Long>();
		while ((line = reader.readLine()) != null) {
			long parsedLine = Long.parseLong(line);
			lineList.add(parsedLine);
		}
		
		List<List<Long>> partitions = Lists.partition(lineList, 100);
		for (List<Long> list : partitions) {
			System.out.println("Lookup up more users...");

			long[] longArray = new long[100];
			int i = 0;
			
			for (Long entry : list) {
				longArray[i] = entry.longValue();
				i++;
			}
			
			
			try {
				ResponseList<User> lookupUsers = twitter.lookupUsers(longArray);
				for (User user : lookupUsers) {
					long id = user.getId();
					String screenName = user.getScreenName();
					String name = user.getName();
					int followersCount = user.getFollowersCount();
					int friendsCount = user.getFriendsCount();
					int realDegree = followersCount + friendsCount;
					int statusesCount = user.getStatusesCount();
					writer.println(String.format("%d\t%s\t%s\t%d\t%d\t%d\t%d", id, screenName, name, followersCount, friendsCount, realDegree, statusesCount));
			        writer.flush();
				}
			} catch (TwitterException e) {
				handleTwitterException(e);
			}
		}
		
		this.close();
	}

}
