package at.sti2.thesis;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;

import com.yahoo.research.bcn.TwitterCrawler;

public class CrawlRemoteIDsTest {
	private static final String USER_ID = "115485051"; // COCO = 115485051
	private static File dir;
	private static final Charset ENCODING = StandardCharsets.UTF_8;
	private static final Path INPUT_FILE = Paths.get("data/input/egoTwitter.recrawl.stratified.ids");
	// private static final Path RECRAWL_STRATIFIED = Paths.get("data/input/egoTwitter.recrawl.stratified.ids");
	
	// 55 days from McAuley2012 forward
	private static long sinceid = Long.parseLong("266056543420698624"); // 266056543420698624 = Nov 7 2012 
	private static long maxid = Long.parseLong("286185261489078272");	// 286185261489078272 = Jan 1 2013
	//private static Paging paging = new Paging(1, 200, sinceid, maxid);
	
	// Same 55 day difference, but from the crawl backward
	private static long sinceid_20130822 = Long.parseLong("365456087677284355"); // Aug 8, 2013 = 365456087677284355
	private static long maxid_20131016 = Long.parseLong("390485923839766528"); // Oct 16, 2013 = 390485923839766528
	private static Paging paging = new Paging(1, 200, sinceid_20130822, maxid_20131016);
	
	// private static Paging paging = new Paging(1, 20);

	@BeforeClass
	public static void oneTimeSetUp() throws ParseException {
		dir = TestHelper.createDataDirectory(INPUT_FILE);
	}

	@Test
	public void testCrawlUserTimelinesFromFile() throws Exception {
		BufferedReader in = Files.newBufferedReader(INPUT_FILE, ENCODING);
		PrintWriter out = new PrintWriter(new StringWriter());
		TwitterCrawler crawler = new TwitterCrawler(in, out);
		
		try {
			while (crawler.hasNextUser()) {
				crawler.nextUser();
				System.out.println("Crawling timeline of user " + crawler.getCurrentUserID());
				boolean done = false;
				while (!done && crawler.retry()) {
					try {
						ResponseList<Status> crawlTimeline = crawler.crawlTimeline(crawler.getCurrentUserID(), getPaging());
						writeRemoteIDsToFile(crawlTimeline, crawler.getCurrentUserID());
						done = true;
					} catch (TwitterException e) {
						done = false;
						crawler.handleTwitterException(e);
					}
				}
				if (!done) {
					System.out.println("Could not access timeline of " + crawler.getCurrentUserID());
				}
			}
		} finally {
			crawler.close();
		}
	}

	private void writeRemoteIDsToFile(ResponseList<Status> crawlTimeline, long currentUserID) throws FileNotFoundException {
		PrintWriter outRemoteIDs = new PrintWriter(new File(dir + "/" + currentUserID + ".remotes"));
		PrintWriter outRemoteIDsWithDates = new PrintWriter(new File(dir + "/" + currentUserID + ".remotes.dates"));
		PrintWriter outNodesAndRemoteIDs = new PrintWriter(new File(dir + "/" + currentUserID + ".node"));
		PrintWriter outTotalRetweets = new PrintWriter(new File(dir + "/" + currentUserID + ".retweets"));
		
		long totalRetweets = 0;
		
		for (Status status : crawlTimeline) {
			if (status.isRetweet()) {
				System.out.println("Skipping retweet...");
				continue;
			}
			System.out.println(status.getId() + " " + status.getText());
			outRemoteIDs.println(status.getId());
			Date createdAt = status.getCreatedAt();
			outRemoteIDsWithDates.println(status.getId() + " " + createdAt);
			outNodesAndRemoteIDs.println(currentUserID + " " + status.getId() + " " + status.getRetweetCount());
			totalRetweets = totalRetweets + status.getRetweetCount();
		}

		// Total Retweets for this user
		outTotalRetweets.println(currentUserID + " " + totalRetweets);
		
		outTotalRetweets.close();
		outRemoteIDsWithDates.close();
		outRemoteIDs.close();
		outNodesAndRemoteIDs.close();
	}

	@Test
	public void testCrawlUserTimeline() throws Exception {
		BufferedReader in = new BufferedReader(new StringReader(USER_ID));
		PrintWriter out = new PrintWriter(new File(USER_ID + ".txt"));
		TwitterCrawler crawler = new TwitterCrawler(in, out);
		crawler.nextUser();

		ResponseList<Status> crawlTimeline = crawler.crawlTimeline(Long.parseLong(USER_ID));
		for (Status status : crawlTimeline) {
			System.out.println("Remote ID: " + status.getId());
			out.println(status.getId());
		}
		crawler.close();
		assertTrue("No results", crawlTimeline.size() > 0);
		System.out.println(crawlTimeline);
	}

	@Test
	public void testCrawlUserTimelineWithPaging() throws Exception {
		BufferedReader in = new BufferedReader(new StringReader(USER_ID));
		PrintWriter out = new PrintWriter(new File(USER_ID + ".txt"));
		TwitterCrawler crawler = new TwitterCrawler(in, out);
		crawler.nextUser();

		// 266056543420698624 // Nov 7 2012
		// COCO's "last" tweet: 383778984669892610
		// COCO on Nov 7 2012: 266274914909167616
		// COCO on Jan 1 2013: 286185261489078272
		// long sinceid = Long.parseLong("266274914909167616");
		Paging paging = new Paging(1, 200, sinceid, maxid);

		ResponseList<Status> crawlTimeline = crawler.crawlTimeline(Long.parseLong(USER_ID), paging);
		for (Status status : crawlTimeline) {
			System.out.println("Remote ID: " + status.getId() + " " + status.getText());
			out.println(status.getId());
		}
		crawler.close();
		assertTrue("No results", crawlTimeline.size() > 0);
		System.out.println(crawlTimeline);
	}
	
	@Test
	public void testCrawlTweetsOfUserOnly() throws IOException, NumberFormatException, TwitterException {
		// String userid = "175760344"; // Camila Monroy @MsMonroy [teenage girl]
		String userid = "17929027"; // Wale Folarin @Wale [has 0 retweets in 10-20-historical set]
		BufferedReader in = new BufferedReader(new StringReader(userid));
		PrintWriter out = new PrintWriter(new StringWriter());
		TwitterCrawler crawler = new TwitterCrawler(in, out);
		Paging paging = new Paging(1, 200, sinceid, maxid);

		ResponseList<Status> crawlTimeline = crawler.crawlTimeline(Long.parseLong(userid), paging);
		for (Status status : crawlTimeline) {
			System.out.println("Remote ID: " + status.getId() + " " + status.isRetweet() + " " + status.getRetweetCount());
		}
		crawler.close();
		assertTrue("No results", crawlTimeline.size() > 0);
	}

	public static Paging getPaging() {
		return paging;
	}
}
