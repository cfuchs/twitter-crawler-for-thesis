package at.sti2.thesis;

import java.io.File;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestHelper {

	public static File createDataDirectory() {
        // Create directory for test-data
        String dirName = "data/" + timestamp();
		return createDir(dirName);
    }

	private static String timestamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss-SSS");
    	Date now = new Date();
        String strDate = sdf.format(now);
		return strDate;
	}

	private static File createDir(String dirName) {
		File dir = new File(dirName);
        boolean success = dir.mkdir();
        if (success) {
			System.out.println("Created data directory " + dir);
		} else {
			System.err.println("ERROR: Directory creation failed!");
		}
        return dir;
	}

	public static File createDataDirectory(Path inputFile) {
		String dirName = "data/" + timestamp();
		return createDir(dirName + " " + inputFile.getFileName());
	}
	
}
