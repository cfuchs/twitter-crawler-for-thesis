package at.sti2.thesis;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Test;

import com.yahoo.research.bcn.TwitterCrawler;

public class RealCrawlerTest {
	private static File dir;
	private static final Charset ENCODING = StandardCharsets.UTF_8;
	private static final Path INPUT_FILE = Paths.get("data/input/top100-nodes-by-degree.txt");
	
    @BeforeClass
    public static void oneTimeSetUp() {
        dir = TestHelper.createDataDirectory();
    }
    
    @Test
    public void testCrawlTop100() throws Exception {
        BufferedReader in = Files.newBufferedReader(INPUT_FILE, ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/top100-recrawl.txt"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.crawl();
	}
    
    @Test
    public void testCrawlLast100() throws Exception {
        BufferedReader in = Files.newBufferedReader(Paths.get("data/input/last100-nodes-by-degree.txt"), ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/last100-recrawl.txt"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.crawl();
	}
    
    @Test
    public void testCrawlTop200to100() throws Exception {
        BufferedReader in = Files.newBufferedReader(Paths.get("data/input/top200-100-nodes.txt"), ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/top200-100-recrawl.txt"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.crawl();
	}
    
    @Test
    public void testCrawlPartition10_100() throws Exception {
        BufferedReader in = Files.newBufferedReader(Paths.get("data/input/wordfreq-10-100.nodes"), ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/wordfreq-10-100.nodes.recrawl"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.crawl();
	}
    
    @Test
    public void testCrawlPartition10_20() throws Exception {
        BufferedReader in = Files.newBufferedReader(Paths.get("data/input/wordfreq-10-20.nodes"), ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/wordfreq-10-20.nodes.recrawl"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.crawl();
	}
    
    @Test
    public void testCrawlPartition10_20Followers() throws Exception {
        BufferedReader in = Files.newBufferedReader(Paths.get("data/input/wordfreq-10-20.nodes"), ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/wordfreq-10-20.nodes.recrawl.followers"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.setCrawlFriends(false);
        crawler.setCrawlFollowers(true);
        crawler.crawl();
	}
    
    @Test
    public void testCrawlPartition10_20FollowersContinued() throws Exception {
        BufferedReader in = Files.newBufferedReader(Paths.get("data/input/wordfreq-10-20.nodes.continued"), ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/wordfreq-10-20.nodes.recrawl.followers.continued"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.setCrawlFriends(false);
        crawler.setCrawlFollowers(true);
        crawler.crawl();
	}

}
