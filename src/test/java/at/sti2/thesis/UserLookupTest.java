package at.sti2.thesis;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Test;

import com.yahoo.research.bcn.TwitterCrawler;

public class UserLookupTest {
	private static File dir;
	private static final Charset ENCODING = StandardCharsets.UTF_8;
	private static final Path INPUT_FILE = Paths.get("data/input/10-20-recrawl-nodes-only.csv");
	private static final Path RECRAWL_STRATIFIED = Paths.get("data/input/egoTwitter.recrawl.stratified.ids");
	
    @BeforeClass
    public static void oneTimeSetUp() {
        dir = TestHelper.createDataDirectory(INPUT_FILE);
    }
    
    @Test
    public void testLookupSeedUsers() throws Exception {
        BufferedReader in = Files.newBufferedReader(INPUT_FILE, ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/userlookup.txt"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.userLookup();
	}
    
    @Test
    public void testLookupRecrawlUsersStratified() throws Exception {
        BufferedReader in = Files.newBufferedReader(RECRAWL_STRATIFIED, ENCODING);
        PrintWriter out = new PrintWriter(new File(dir + "/userlookup.txt"));
        TwitterCrawler crawler = new TwitterCrawler(in, out);
        crawler.userLookup();
	}
    
}
