package at.sti2.thesis;

import java.io.IOException;

import org.junit.Test;

public class DataPartionerTest {

	private static final String INPUT_FILE = "data/input/wordfreq.txt";
	private static final String INPUT_FILE_RECRAWL = "data/input/egoTwitter.recrawl.sorted.by.realdeg.csv";
	
	@Test
	public void test() throws IOException {
		DataPartitioner dataPartitioner = new DataPartitioner(INPUT_FILE, 10, 20);
		dataPartitioner.partition();
		dataPartitioner.writePartitionsToFiles();
	}
	
	@Test
	public void partitionRecrawlData() throws IOException {
		DataPartitioner dataPartitioner = new DataPartitioner(INPUT_FILE_RECRAWL, 10, 20);
		dataPartitioner.partition();
		dataPartitioner.writePartitionsToFiles();
	}

}
