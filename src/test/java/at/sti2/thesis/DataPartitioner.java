package at.sti2.thesis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;
import com.yahoo.research.bcn.Utils;

public class DataPartitioner {

	private static final Charset ENCODING = StandardCharsets.UTF_8;
	
	private int N;

	private String filename;

	private int p;

	private boolean random = true;

	private List<List<String>> partitions;

	public DataPartitioner(String filename, int p, int N) throws IOException {
		this.N = N;
		this.p = p;
		this.filename = filename;
	}

	public void partition() throws IOException {
		System.out.println("Partitioning " + filename + " using " + p
				+ "% partitions, taking the first " + N
				+ " data points of each partition.");

		// Get total number of lines
		int numLines = Utils.countLines(filename);
		int partitionSize = numLines / (100 / p);
		int numPartitions = numLines / partitionSize;

		System.out.println(numLines + " = " + numPartitions + " * "
				+ partitionSize);

		Path path = Paths.get(filename);
		List<String> allLines = Files.readAllLines(path, ENCODING);
		partitions = Lists.partition(allLines, partitionSize);
	}

	public void writePartitionsToFiles() throws IOException {
		int i = 0;
		for (List<String> partition : partitions) {
			i++;
			System.out.println(partition);
			String partitionFile = filename + "." + p + "-" + N + "." + i; // file.10-100.1
			System.out.println("Writing to " + partitionFile);
			if (isRandom())
				Collections.shuffle(partition);
			writeToFile(partitionFile , partition);
			System.out.println("---------------------");
		}
	}
	
	private void writeToFile(String filename, List<String> partition)
			throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
		
		int i = 0;
		for (String str : partition) {
			i++;
			if (i >= N)
				break;
			
			bw.write(str);
			bw.newLine();
		}
		bw.close();
	}

	public boolean isRandom() {
		return random;
	}

	public void setRandom(boolean random) {
		this.random = random;
	}
}
